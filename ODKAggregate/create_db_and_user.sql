-- Adapt this code to your conf

create database "odk_database_name";
create user "odk_user_name" with password 'odk_user_name';
grant all privileges on database "odk_database_name" to "odk_user_name";
alter database "odk_database_name" owner to "odk_user_name";

-- Connect to ODK DATABASE called odk_database_name
\c "odk_database_name";

create schema "odk_schema_name";
grant all privileges on schema "odk_schema_name" to "odk_user_name";
alter schema "odk_schema_name" owner to "odk_user_name";
      