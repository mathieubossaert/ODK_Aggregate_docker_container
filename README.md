This project is outdated. If you plan to setup a Aggregate docker image, take a look at this discussion on opendatakit's forum :
https://forum.opendatakit.org/t/odk-aggregate-docker-image-for-postgresql/13562/33

![](pictures/ODK_Logo.png)

A docker image running ODK Aggregate over tomcat8 and postgresql 10
Adapted from https://github.com/Kharatsa/odkaggregate

Get the archive on your computeur :
```
wget https://framagit.org/mathieubossaert/ODK_Aggregate_docker_container/-/archive/master/ODK_Aggregate_docker_container-master.zip
```
unzip it
move to the uncompressed directory
Build the image, it could take a while
```
docker build -t "sicen/odk_aggregate:1.5.0" .
```

Adapt and run the SQL command from the file to create if needed Database / User and Schema needed
```sql
-- Adapt this code to your conf

create database "odk_database_name";
create user "odk_user_name" with password 'odk_user_name';
grant all privileges on database "odk_database_name" to "odk_user_name";
alter database "odk_database_name" owner to "odk_user_name";

-- Connect to ODK DATABASE called odk_database_name
\c "odk_database_name";

create schema "odk_schema_name";
grant all privileges on schema "odk_schema_name" to "odk_user_name";
alter schema "odk_schema_name" owner to "odk_user_name";
      
```

Store host IP address into a variable and run a container (called "aggregate" in this exemple)

```
ODK_HOSTNAME=$(ifconfig eth0 | grep -oP 'inet addr:\K\S+')
docker run -d --name chaton -p 8080:8080 \
    -e DB_CONTAINER_NAME=pirate_postgis_1 \
    -e PGSQL_DATABASE=odk_database_name \
    -e PGSQL_SCHEMA=odk_schema_name \
    -e PGSQL_USER=odk_database_user_name \
    -e PGSQL_PASSWORD=odk_database_user_password \
    -e ODK_ADMIN_USER_EMAIL=admin@toto.com \
    -e ODK_PORT=8080 \
    -e ODK_HOSTNAME=$ODK_HOSTNAME \
    -e ODK_PORT_SECURE=8888 \
    sicen/odk_aggregate:1.5.0 \
    && docker logs --follow aggregate
```

Environnement variables used are :
* DB_CONTAINER_NAME=pirate_postgis_1  -- the name of the postgresql database container. Think it should be replce by th IP address av th PG server
* PGSQL_DATABASE=odk_prod -- database name
* PGSQL_SCHEMA=odk_prod -- odk schema within th database
* PGSQL_USER=odk_boss -- pgsql user
* PGSQL_PASSWORD=odk_boss -- odk user's password
* ODK_ADMIN_USER_EMAIL=mailto:sig@cenlr.org -- odk admin user email
* ODK_PORT=8080 -- odk port (generally the tomcat one)
* ODK_PORT_SECURE=8888 -- odk secure port (not tested)
* ODK_HOSTNAME=$ODK_HOSTNAME --odk hostname, could be a hard known value or given by the $ODK_HOSTNAME variable ($(ifconfig eth0 | grep -oP 'inet addr:\K\S+'))gg