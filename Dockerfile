# derived and inspired from https://github.com/Kharatsa/odkaggregate
FROM debian:jessie

MAINTAINER Mathieu Bossaert

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get -y upgrade
RUN apt-get -y install apt-utils
RUN apt-get -y install openjdk-7-jdk
RUN apt-get -y install wget
RUN apt-get -y install pwgen
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV TOMCAT_MAJOR_VERSION 8
ENV TOMCAT_MINOR_VERSION 8.0.52
ENV CATALINA_HOME /tomcat
# To be improve : ODKAggregate IP as a variable
ENV ODK_PORT='8080'
ENV ODK_HOSTNAME='192.168.1.131'
ENV ODK_ADMIN_USERNAME='odk_admin'
ENV ODK_ADMIN_USER_EMAIL='mailto:toto@gmail.com'
ENV ODK_AUTH_REALM='ODK Aggregate'

# TOMCAT INSTALL 
RUN wget https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz && \
    tar zxf apache-tomcat-*.tar.gz && \
    rm apache-tomcat-*.tar.gz && \
    mv apache-tomcat* tomcat

# PostregSQL JDBC driver. See https://jdbc.postgresql.org/download.html
# Need to be more generic...
RUN wget https://jdbc.postgresql.org/download/postgresql-42.2.2.jar && \
    mv postgresql-42.2.2.jar ${CATALINA_HOME}/lib/

# ODK Aggregate WAR archive
#RUN wget si.cenlr.org/sites/si.cenlr.org/files/ODKAggregate.war 
COPY ODKAggregate/ODKAggregate.war .
    
# create_tomcat_admin_user.sh
#RUN wget https://framagit.org/odk/formation_odk/blob/create_tomcat_admin_user.sh 
COPY create_tomcat_admin_user.sh .

# run.sh
#RUN wget https://framagit.org/odk/formation_odk/blob/dev/run.sh
COPY run.sh .

RUN chmod +x /*.sh

EXPOSE ${ODK_PORT}

ENTRYPOINT ["/run.sh"]
